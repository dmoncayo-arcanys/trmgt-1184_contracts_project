﻿using Autofac;
using ContosoUniversity.Repositories;
using ContosoUniversity.Services;
using System.Reflection;

namespace ContosoUniversity.Composition.Installers
{
    public class ManagerInstaller
	{
		private readonly ContainerOptions _options;

		public ManagerInstaller(ContainerOptions options)
		{
			_options = options;
		}

		public void Install(ContainerBuilder builder)
		{
			var ServiceAssembly = typeof(InitService).GetTypeInfo().Assembly;
			var RepositoryAssembly = typeof(InitRepository).GetTypeInfo().Assembly;
			builder
				.RegisterAssemblyTypes(ServiceAssembly, RepositoryAssembly)
				.AsSelf()
				.AsImplementedInterfaces();
		}
	}
}
