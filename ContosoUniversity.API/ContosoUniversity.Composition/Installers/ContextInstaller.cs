﻿using Autofac;
using ContosoUniversity.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ContosoUniversity.Composition.Installers
{
    public class ContextInstaller
	{
		private readonly ContainerOptions _options;

		public ContextInstaller(ContainerOptions options)
		{
			_options = options;
		}

		public void Install(ContainerBuilder builder)
		{
			builder.Register(x =>
			{
				var config = x.Resolve<IConfiguration>();
				var optionsBuilder = new DbContextOptionsBuilder<SchoolDbContext>();
				optionsBuilder.UseSqlServer(config.GetConnectionString("DbContext"));
				return new SchoolDbContext(optionsBuilder.Options);
			}).InstancePerLifetimeScope();
		}
	}
}
