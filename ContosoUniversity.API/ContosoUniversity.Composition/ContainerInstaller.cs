﻿using Autofac;
using ContosoUniversity.Composition.Installers;

namespace ContosoUniversity.Composition
{
    public class ContainerInstaller
	{
		private readonly ContainerOptions _options;

		public ContainerInstaller(ContainerOptions options)
		{
			_options = options;
		}

		public ContainerBuilder Install()
		{
			var builder = new ContainerBuilder();

			new ManagerInstaller(_options).Install(builder);
			new ContextInstaller(_options).Install(builder);

			return builder;
		}
	}
}
