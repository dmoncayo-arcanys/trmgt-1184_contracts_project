﻿using ContosoUniversity.Contracts.Repository;
using System.Collections.Generic;

namespace ContosoUniversity.Contracts.Service
{
    public interface IActionManager<T> where T : class
    {
        IUnitOfWork UnitOfWork { get; }
        IEnumerable<T> GetAll();
        T Get(int? id);
        bool Insert(T entity);
        bool Update(T entity);
        bool Delete(int? id);
        void SaveChanges();
    }
}
