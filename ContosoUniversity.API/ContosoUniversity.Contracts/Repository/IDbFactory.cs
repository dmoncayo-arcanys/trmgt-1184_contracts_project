﻿namespace ContosoUniversity.Contracts.Repository
{
    public interface IDbFactory<T> where T : class
    {
        T GetDbContext { get; }
    }
}
