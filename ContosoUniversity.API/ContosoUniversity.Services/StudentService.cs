﻿using ContosoUniversity.Contracts.Entities;
using ContosoUniversity.Contracts.Repository;
using ContosoUniversity.Contracts.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Services
{
    public class StudentService : IActionManager<Student>
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository repository;
        private readonly ILogger<StudentService> logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public StudentService(IUnitOfWork unitOfWork, IRepository repository, ILogger<StudentService> logger)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.logger = logger;
        }

        /// <summary>
        /// IUnitOfWork init.
        /// </summary>
        public IUnitOfWork UnitOfWork => unitOfWork;

        /// <summary>
        /// Delete student record.
        /// </summary>
        /// <param name="id"></param>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                var student = repository.All<Student>().FirstOrDefault(m => m.ID == id);
                if (student != null)
                {
                    repository.Delete(student);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Get student by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Student</returns>
        public Student Get(int? id)
        {
            Student student = null;
            try
            {
                student = repository.All<Student>()
                    .AsNoTracking()
                    .FirstOrDefault(m => m.ID == id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return student;
        }

        /// <summary>
        /// Get students.
        /// </summary>
        /// <returns>IEnumerable<Student></returns>
        public IEnumerable<Student> GetAll()
        {
            IEnumerable<Student> students = null;
            try
            {
                students = repository.All<Student>()
                    .AsNoTracking();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return students;
        }

        /// <summary>
        /// Insert new record.
        /// </summary>
        /// <param name="entity"></param>
        public bool Insert(Student entity)
        {
            bool flag = true;
            try
            {
                if (entity != null)
                {
                    repository.Create(entity);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Save changes.
        /// </summary>
        public void SaveChanges()
        {
            UnitOfWork.SaveChanges();
        }

        /// <summary>
        /// Update student record.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>bool</returns>
        public bool Update(Student entity)
        {
            bool flag = true;
            try
            {
                var find = repository.All<Student>().FirstOrDefault(m => m.ID == entity.ID);
                if (find != null)
                {
                    repository.Update(entity);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }
    }
}
