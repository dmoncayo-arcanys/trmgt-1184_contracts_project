﻿using ContosoUniversity.Contracts.Repository;
using System;

namespace ContosoUniversity.Repositories
{
    public class SchoolDbFactory : IDbFactory<SchoolDbContext>, IDisposable
    {

        private readonly SchoolDbContext context;

        public SchoolDbFactory(SchoolDbContext context)
        {
            this.context = context;
        }

        public SchoolDbContext GetDbContext
        {
            get
            {
                return context;
            }
        }

        private bool isDisposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                }
            }
            isDisposed = true;
        }
    }
}
