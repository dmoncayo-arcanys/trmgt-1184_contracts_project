﻿using ContosoUniversity.Contracts.Repository;

namespace ContosoUniversity.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory<SchoolDbContext> dbFactory;

        public UnitOfWork(IDbFactory<SchoolDbContext> dbFactory)
        {
            this.dbFactory = dbFactory;
        }

        public void BeginTransaction()
        {
            dbFactory.GetDbContext.Database.BeginTransaction();
        }

        public void RollbackTransaction()
        {
            dbFactory.GetDbContext.Database.RollbackTransaction();
        }

        public void CommitTransaction()
        {
            dbFactory.GetDbContext.Database.CommitTransaction();
        }

        public void SaveChanges()
        {
            dbFactory.GetDbContext.Save();
        }
    }
}
