﻿using ContosoUniversity.API.Models;
using ContosoUniversity.Contracts.Entities;
using ContosoUniversity.Contracts.Service;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly IActionManager<Student> studentService;

        /// <summary>
        /// Controller.
        /// </summary>
        /// <param name="studentService"></param>
        public StudentsController(IActionManager<Student> studentService)
        {
            this.studentService = studentService;
        }

        /// <summary>
        /// GET: /Students
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpGet]
        public IActionResult GetStudents()
        {
            ApiResultModel results = new();
            results.Response = studentService.GetAll();
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// GET: api/Students/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IActionResult</returns>
        [HttpGet("{id}")]
        public IActionResult GetStudent(int? id)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = studentService.Get(id);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// POST: api/Students
        /// </summary>
        /// <param name="student"></param>
        /// <returns>IActionResult</returns>
        [HttpPost]
        public IActionResult PostStudent(Student student)
        {
            ApiResultModel results = new();
            results.Response = studentService.Insert(student);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// PUT: api/Students/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="student"></param>
        /// <returns>IActionResult</returns>
        [HttpPut("{id}")]
        public IActionResult PutStudent(int? id, Student student)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = studentService.Update(student);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// DELETE: api/Students/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IActionResult</returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteStudent(int? id)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = studentService.Delete(id);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }
    }
}
