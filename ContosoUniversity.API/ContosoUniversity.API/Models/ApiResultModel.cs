﻿namespace ContosoUniversity.API.Models
{
    public class ApiResultModel : BaseApiResultModel
    {
        public string ErrCode { get; set; }

        public string SuccCode { get; set; }

    }
}
